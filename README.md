## Instructions
# To run the backend:
 - npm install
 - node server
 - The backend connects to an external database hosted in mlab, which already has the data you provided in the test description so you can query it.

Once the backend is running you can hit any of these endpoints at http://localhost:8000
 ```method: 'GET', path: '/v1/orders/company'

	method: 'GET', path: '/v1/orders/address'

	method: 'DELETE', path: '/v1/order/{orderId}'

	method: 'GET', path: '/v1/orders/ocurrences'

	method: 'POST', path: '/v1/order', body: {orderId: xxx, companyName: 'aaa', customerAddress: 'ssss', orderItem: 'gggg'}```

# To run the frontend:
  - npm install
  - npm run dev

## Questions

Why did you pick your particular your design? What assumptions did you make, and what tradeoffs did you consider?
- Since it’s a small application with very specific functionality a very straightforward architecture was enough to supply its need, we only were considering one business object, no security and so on so a backend separating endpoint handlers (controllers) from the data layer (schemas) was good enough for it to work properly and still maintain a good order and readability. As for tradeoffs, the fact of considering a more complex architecture and a more solid database (and the decision of having the database hosted elsewhere to make it easier to test since it already has data on it)

What do you like (and dislike) about Node/Javascript compared to other programming languages?
- Node/Javascript is a great language and probably my favorite (specially with ES6 features), I like how easily you can do things, create objects, handle collections of things, and so on. This obviously becomes a double edge sword, the language being so free and lack of a lot of rules makes it easy for new comers to make mistakes and write bad code, not only with bad functionality but poorly written as opposed to more strictly typed languages like C# or Java (and don’t get me wrong, people can write bad things in any language but these make it harder). 

