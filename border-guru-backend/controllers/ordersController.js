var Order = require('../data/orderModel');

exports.getCompanyOrders = {
    handler: function(request, reply) {
      return reply(Order.find({companyName: request.query.companyName}))
    }
  };

exports.getOrdersToAddress = {
  handler: function(request, reply) {
    return reply(Order.find({customerAddress: request.query.customerAddress}));
  }
};

exports.deleteOrderById = {
  handler: function(request, reply) {
    Order.find({orderId: request.params.orderId}).remove(function(){
      return reply('Order ' + request.params.orderId + ' deleted successfully');
    })
  }
};

exports.countOcurrences = {
  handler: function(request, reply) {
    return reply(Order.aggregate([
      {$group: {_id: "$orderItem", count : {$sum : 1}}},
      {$sort: {count: -1}},
      {$project: {orderItem: "$_id", _id: 0, count: "$count"}}
    ]));
  }
};

exports.createOrder = {
  handler: function(request, reply) {
     var order = new Order({
        orderId : request.payload.orderId,
        companyName: request.payload.companyName,
        customerAddress : request.payload.customerAddress,
        orderItem: request.payload.orderItem
     });
     order.save(function(err){
       return reply('Order created successfully')
     });
  }
};

