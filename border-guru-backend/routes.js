var ordersController = require('./controllers/ordersController');

exports.endpoints = [{method: 'GET', path: '/', config: {handler: function(request, reply){reply('API v1, Orders')}}},
	{method: 'GET', path: '/v1/orders/company', config: ordersController.getCompanyOrders},
	{method: 'GET', path: '/v1/orders/address', config: ordersController.getOrdersToAddress},
	{method: 'DELETE', path: '/v1/order/{orderId}', config: ordersController.deleteOrderById},
	{method: 'GET', path: '/v1/orders/ocurrences', config: ordersController.countOcurrences},
	{method: 'POST', path: '/v1/order', config: ordersController.createOrder}
];
