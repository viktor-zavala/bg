var mongoose = require('mongoose');

var OrderModel = new mongoose.Schema({
  orderId : String,
  companyName: String,
  customerAddress : String,
  orderItem: String

});

module.exports = mongoose.model('Order', OrderModel);